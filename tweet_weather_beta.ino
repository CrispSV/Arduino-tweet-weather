/* Post a simple message to Twitter  */
/* Chris Pap ** drmac.gr    **********/
//   BETA TEST 17/10/2016 

#include <SPI.h>
#include <Ethernet.h>
#include <Twitter.h>
#include <DHT.h>
  DHT dht;


int DHTread = 0; // set DHTread as an integer variable
int temp = 0; // set temp as integer variable
int humid = 0; // set humid as integer variable
int sensorCheck = 0;

int i=1;

char buf[100];


byte mac[] = { 0x90, 0xA2, 0xDA, 0x0E, 0x92, 0xA4 };
//byte ip[] = { 192, 168, 1, 1 };    obsolete if using DHCP

 
Twitter twitter("abcdefg121234123412323123123"); // Twitter token


void setup()
{
  
   dht.setup(6); // DHT22 data pin 6
  
  Ethernet.begin(mac); // Ethernet.begin(mac, ip)    if using static IP
  Serial.begin(9600);
}


void tweet(char msg[]){
  
   Serial.println("connecting ...");
  if (twitter.post(msg)) {
    int status = twitter.wait(&Serial);
    if (status == 200) {
      Serial.println("OK.");
    } else {
      Serial.print("failed : code ");
      Serial.println(status);
    }
  } else {
    Serial.println("connection failed.");
  }

}


void loop()
{
  delay(dht.getMinimumSamplingPeriod());
  int humid = dht.getHumidity();
  int temp = dht.getTemperature();


  sprintf(buf, "Current room Temp: %dC Humidity: %d%%  TweetNo: %d via an #Arduino", temp, humid, i );
  
  
    Serial.print( buf);
  
  tweet (buf);
    i++; 
  
  delay(60000);
}
