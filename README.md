Arduino Tweet Weather Data
==========================

Arduino code for tweeting weather data via ethernet shield. Using DHT sensor and NeoCat twitter library for posting tweets. Note that NeoCat library is updated.
https://github.com/JChristensen/Twitter
